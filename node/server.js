//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;
var bodyParser =require('body-parser') ;
app.use(bodyParser.json());

const cors = require('cors');
//const config = require('./config');


const config ={
    application:{
      cors:{
        server:[
          {
            origin: "localhost:3000",
            credentials: true
          }
        ]
      }
    }
};

app.use(cors(
    config.application.cors.server
));
app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin,X-Requested-With, Content-Type, Accept");
  next();
});
var requestJson = require('request-json');
var urlCapacidadesMLab = "https://api.mlab.com/api/1/databases/hackatondb/collections/capacidades?apiKey=j7EFU8TsmOhd8S4Ncwx7e7rpLMjku1IP";
var capacidadesMLab = requestJson.createClient(urlCapacidadesMLab);

//requestJson = require('request-json');
//var urlUsuariosMLab = " ";

requestJson = require('request-json');
var urlUsuariosMLab = "https://api.mlab.com/api/1/databases/hackatondb/collections/basicaClientes?apiKey=j7EFU8TsmOhd8S4Ncwx7e7rpLMjku1IP";
var usuariosMLab = requestJson.createClient(urlUsuariosMLab);

requestJson = require('request-json');
var urlAnaquelTdcMLab = "https://api.mlab.com/api/1/databases/hackatondb/collections/anaquelTdc?apiKey=j7EFU8TsmOhd8S4Ncwx7e7rpLMjku1IP";
var anquelTdcMLab = requestJson.createClient(urlAnaquelTdcMLab);

requestJson = require('request-json');
var urlAnaquelConsumoMLab = "https://api.mlab.com/api/1/databases/hackatondb/collections/anaquelConsumo?apiKey=j7EFU8TsmOhd8S4Ncwx7e7rpLMjku1IP";
var anaquelConsumoMLab = requestJson.createClient(urlAnaquelConsumoMLab);

requestJson = require('request-json');
var urlAnaquelAutoMLab = "https://api.mlab.com/api/1/databases/hackatondb/collections/anaquelAuto?apiKey=j7EFU8TsmOhd8S4Ncwx7e7rpLMjku1IP";
var anaquelAutoMLab = requestJson.createClient(urlAnaquelAutoMLab);

var path = require('path');

app.listen(port);


console.log('todo list RESTful API server started on: ' + port);

app.get("/", function(req, res){
  //__dirname incica que lo tome del proyecto donde estoy parado
  res.sendFile(path.join(__dirname,'index.html'));
});





app.post("/cliente/login",function(req, res) {

    console.log("cliente recibido: "+ req.body.cliente);
    console.log("password recibido: "+ req.body.password);

    if(!req.body.cliente || !req.body.password)
    {
      res.send( JSON.stringify({"statusCode":400, "error":"El cliente y el password no pueden ir vacíos" }));
    }else {

         var cliente= req.body.cliente;
         var password= req.body.password;
         var filtros = '&q={"CD_CLIENTE":"'+cliente+'","CD_PASSWORD":"'+password+'"}';

         console.log ("filtros: "+filtros);
         urlUsuariosMLabQ= urlUsuariosMLab+filtros;
        var usuariosMLab = requestJson.createClient(urlUsuariosMLabQ);

            usuariosMLab.get('', function(err, resM, body){
                           console.log("body length: "+JSON.stringify(resM.body.length));
                           console.log("resM"+resM);
                           console.log("resM.body"+resM.body);
                           console.log("body"+JSON.stringify(body));
                           console.log("statusCode " + resM.statusCode);

                console.log ("respuesta de busqueda de usuario: " + resM.body );

              //  console.log("antes de validaciòm "+cliente +" "+password);

                if(!err){
                  if (body.length > 0){
                   res.send( JSON.stringify({"statusCode":resM.statusCode ,"descripcion":"Login Ok"}));
              //  res.send(body);
                   console.log("Enviando OK");
                 } else {
                   res.send( JSON.stringify({"statusCode":204, "descripcion":"Usuario y password incorrectos" }));
                     console.log("Enviando error");

                   }
                  } else {
                    console.log("statusCode: "+ resM.statusCode +" error : "+err);
                  res.send( JSON.stringify({"statusCode":204, "descripcion":"Usuario y password incorrectos" }));
                    console.log("Enviando error");

                  }
              });
    }
  });

app.post("/simular",function(req, res) {
  var cliente= req.body.cliente;
  //1 y 3 Si el cleinte existe y no tiene decantamientos
  var filtros = '&q={"CD_CLIENTE":"'+cliente+'" ,"ST_DECANTAMIENTO":{$nin: [2,3,8,10,11,13,14]}}';
  var alternativas={"CD_CLIENTE":cliente};

  console.log ("filtros: "+filtros);
  urlUsuariosMLabQ= urlUsuariosMLab+filtros;
  var usuariosMLab = requestJson.createClient(urlUsuariosMLabQ);
     usuariosMLab.get('', function(err, resM, body){
            console.log("body length: "+JSON.stringify(resM.body.length));
            console.log("body size: "+body.length);
            console.log("Cliente"+resM.body);
        //    resultado = resM.body;
        console.log("****** producto: "+body[0].NB_SEGTO_FIJO);
        alternativas.NB_SEGTO_FIJO=body[0].NB_SEGTO_FIJO;
        alternativas.TP_CONSUMO=body[0].TP_CONSUMO;

    // jsons.push(JSON.stringify(resM.body));

  });
  // 3. Capacidad de Pago > 0
  filtros = '&q={"CD_CLIENTE":"'+cliente+'"}'; //, "IM_CAP_PAGO":{$gt:0}}'; //, "CD_PRODUCTO": "body[0].TP_PRODUCTO"'; // '+resM.body.TP_PRODUCTO +'" }';
console.log ("filtros capacidades: "+filtros);
  urlCapacidadesMLabQ = urlCapacidadesMLab+filtros;
  capacidadesMLab = requestJson.createClient(urlCapacidadesMLabQ);
  capacidadesMLab.get('', function(err, resC, bodyC){

  //Ejemplo alternativas.CD_ALTERNATIVAS = {"CD_SUBPRODUCTO":"TDC", "IMPORTE":"1234"};
  console.log("body size: "+bodyC.length);
  console.log("capacidades"+resC.bodyC);
  console.log("capacidades body"+ bodyC);
  var i,n=0;
  var productos=new Array;
  for(i=0; i<bodyC.length; i++){

    //CALCULAR importeAlternativa
    //Plazo auto=60 tdc=24, consumo sacar de la tabla del producto
    //Pago mensual solo para consumo(0NOM Y 0PPI) y auto
    var importeAlternativa=0;
    var pagoMensual=0;
    var plazo =0;
    //Para CONSUMO VALIDAR QUE  TP_CONSUMO(clientes) = CD_PRODUCTO (capacidades)
      if (bodyC[i].CD_PRODUCTO == "0NOM") {
        //console.log ("alternativas.TP_CONSUMO "+ alternativas.TP_CONSUMO);
        if (alternativas.TP_CONSUMO=="NOM"){
          plazo =3; //sacar de tabla
          importeAlternativa = resuelveFormulaCalculo (bodyC[i].IM_CAP_PAGO,2 ,plazo);
          pagoMensual = importeAlternativa/plazo; //SObre plazo
        } else { continue;}
      }else if (bodyC[i].CD_PRODUCTO == "0PPI") {
        if (alternativas.TP_CONSUMO =="PPI"){
        plazo =3; //sacar de tabla
        importeAlternativa = resuelveFormulaCalculo (bodyC[i].IM_CAP_PAGO,2 ,plazo);
        pagoMensual = importeAlternativa/plazo; //SObre plazo
      } else { continue;}
      }else if (bodyC[i].CD_PRODUCTO == "0TDC") {
        plazo =24;
        importeAlternativa = resuelveFormulaCalculo (bodyC[i].IM_CAP_PAGO,1 ,plazo);
      }else if (bodyC[i].CD_PRODUCTO == "AUTO") {
        plazo=60;
        importeAlternativa = resuelveFormulaCalculo (bodyC[i].IM_CAP_PAGO,1 ,plazo);
        pagoMensual = importeAlternativa/plazo;
      }

     productos[n] = {"CD_PRODUCTO": bodyC[i].CD_PRODUCTO,
                     "IM_CAP_PAGO": bodyC[i].IM_CAP_PAGO,
                     "IMPORTE_PAGO": pagoMensual,
                     "IMPORTE": importeAlternativa,
                     "PLAZO": plazo
                   }
    n++;
  }
  alternativas.productos = productos;

console.log("Alternativas:::" + alternativas);
  res.send(alternativas);
});

function resuelveFormulaCalculo(capacidad, tasa, plazo){

		 //return capacidad*(((Math.pow(((1+(tasa/12)),(plazo)))-(1))/((tasa/12)*(Math.pow((1+(tasa/12)),plazo))));
     tasa =tasa/100;
     var resu = (Math.pow(1+(tasa/12),plazo))-1;
		 var resu3 = tasa/12;
		 var resu4 = resu3*(Math.pow(1+(tasa/12),plazo));
		 var resu5 = capacidad*resu/resu4;

		   return resu5;

  }


});

  if(process.env.NODE_ENV !== 'production') {
    process.once('uncaughtException', function(err) {
      console.error('FATAL: Uncaught exception.');
      console.error(err.stack||err);
      setTimeout(function(){
        process.exit(1);
      }, 100);
    });
  };
